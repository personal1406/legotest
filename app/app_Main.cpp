#include "app.hpp"

int main(int /*argc*/, char ** /*argv*/)
{
    std::cout << "In main" << std::endl;
    rapidjson::Document masterDataJson = application::getJsonDoc("../app/masterData.json");
    rapidjson::Document itemJson = application::getJsonDoc("../app/Item.json");
    rapidjson::Document brickJson = application::getJsonDoc("../app/listOfBricks.json");

    std::list<MasterData> master = application::MasterDataService(masterDataJson);
    std::list<Item> items = application::ItemDataService(itemJson);
    std::list<Brick> listOfBricks = application::parseListOfBricks(brickJson);

    Item myItem = application::PreferredItemService(listOfBricks, master, items);
    std::cout << " Preferred Item is " << myItem.getItemUiD() << std::endl;
    return 0;
}