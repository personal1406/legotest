#include "brick.hpp"

Brick::Brick() {}

Brick::Brick(int i_designId) { m_designId = i_designId; }

Brick::Brick(int i_designId, std::list<int> i_brickColors) {
  setDesignId(i_designId);
  setColorCodes(i_brickColors);
}

Brick::~Brick() {}

void Brick::setDesignId(int i_designId) { m_designId = i_designId; }

void Brick::setColorCodes(std::list<int> i_brickColors) {
  for (int n : i_brickColors) {
    m_colorCodes.push_back(static_cast<ColorCode>(n));
  }
}

std::list<Brick::ColorCode> Brick::getColorCodes() { return m_colorCodes; }

int Brick::getDesignId() { return m_designId; }