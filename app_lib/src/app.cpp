#include "app.hpp"

namespace application
{
    rapidjson::Document getJsonDoc(std::string filePath)
    {

        rapidjson::Document jsonDoc;
        std::cout << filePath << std::endl;
        FILE *file = fopen(filePath.c_str(), "r");

        if (!file)
        {
            std::cout << "error" << std::endl;
        }

        char readBuffer[65536];
        rapidjson::FileReadStream readStream(file, readBuffer, sizeof(readBuffer));
        jsonDoc.ParseStream(readStream);
        fclose(file);

        return jsonDoc;
    }

    Brick brickParser(const rapidjson::Value &i_json)
    {
        // Initialize an empty brick that will be parsed correctly only if json has all fields
        Brick brickElement;

        if (i_json.HasMember("designId") && i_json.HasMember("colorCodes"))
        {
            brickElement.setDesignId(i_json["designId"].GetInt());
            const auto &colorsObj = i_json["colorCodes"].GetArray();
            if (colorsObj.Size() == 0)
            {
                throw "No Color Codes! ";
            }
            std::list<int> colorCodes{};
            for (auto &color : colorsObj)
            {
                colorCodes.push_back(color.GetInt());
            }
            brickElement.setColorCodes(colorCodes);
        }
        return brickElement;
    }

    std::list<int> matchBricksListToItem(std::list<Brick> i_listOfBricks, std::list<Item> i_itemList)
    {
        // We need the current UiDs for the items to then later search in the MasterData
        std::list<int> currentUiDs;
        for (Item item : i_itemList)
        {

            bool possibleMatch = false;
            // Get all bricks in the item, we need to compare all of them to the comparison bricks
            std::list<Brick> currentItemBricks = item.getBricksInItem();

            // We first check the number of bricks, we need exact match
            if (currentItemBricks.size() == i_listOfBricks.size())
            {

                // Select the first brick to compare to
                for (Brick currentCompareBrick : i_listOfBricks)
                {
                    // Order here does not matter
                    for (Brick currentBrick : currentItemBricks)
                    {
                        if (currentBrick.getDesignId() == currentCompareBrick.getDesignId() && currentBrick.getColorCodes() == currentCompareBrick.getColorCodes())
                        {
                            // Might be a match!
                            possibleMatch = true;
                        }
                        else
                        {
                            possibleMatch = false;
                        }
                    }
                }
                if (possibleMatch)
                {
                    // If all possible bricks in item match the wanted bricks list, save the UiD
                    currentUiDs.push_back(item.getItemUiD());
                }
            }
            else
            {
                // Jump to next item
                continue;
            }
        }
        return currentUiDs;
    }

    int findBestStatusPriceItem(std::list<MasterData> i_masterData, std::list<int> i_itemUiD)
    {
        int bestUiD;
        std::list<MasterData> possibleMatches;
        for (int UiD : i_itemUiD)
        {

            for (MasterData data : i_masterData)
            {

                // Search now for the data in MasterData
                if (data.getUiD() == UiD)
                {
                    possibleMatches.push_back(data);
                }
            }
        }
        // Initialize to lowest possibleStatus
        int currentStatus = 0;
        std::list<MasterData> tempItems{};

        // Search for best Status
        for (MasterData data : possibleMatches)
        {
            if (data.getStatus() >= currentStatus)
            {
                // find best status
                currentStatus = data.getStatus();
            }
        }
        // create a list of items with best status
        for (MasterData data : possibleMatches)
        {
            if (data.getStatus() == currentStatus)
            {
                tempItems.push_back(data);
            }
        }
        // Search for lowest price
        // We can get the max value from somewhere else, here just initialized to the highest value
        float tempPrice = 9999999;
        for (MasterData data : tempItems)
        {
            if (data.getPrice() < tempPrice)
            {
                tempPrice = data.getPrice();
            }
        }
        // Select now the best Status, lowest price
        for (MasterData data : tempItems)
        {
            if (data.getPrice() <= tempPrice)
            {
                bestUiD = data.getUiD();
            }
        }

        return bestUiD;
    }

    std::list<Brick> parseListOfBricks(const rapidjson::Value &i_json)
    {
        std::list<Brick> listOfBricks;
        if (i_json.IsObject() && i_json.HasMember("listBricks"))
        {
            const auto &listObj = i_json["listBricks"].GetArray();
            for (const auto &brick : listObj)
            {
                if (brick.HasMember("designId") && brick.HasMember("colorCodes"))
                {
                    listOfBricks.push_back(brickParser(brick));
                }
            }
        }
        return listOfBricks;
    }

    std::list<MasterData> MasterDataService(const rapidjson::Value &i_json)
    {
        // Initialize the empty list that will be parsed only if json has all fields
        std::list<MasterData> dataList{};

        if (i_json.IsObject() && i_json.HasMember("MasterData"))
        {

            const auto &dataObj = i_json["MasterData"].GetArray();
            if (dataObj.Size() == 0)
            {
                throw "No Master Data! ";
            }
            for (const auto &data : dataObj)
            {

                if (data.HasMember("UiD") && data.HasMember("price") &&
                    data.HasMember("status"))
                {

                    int currentUid = data["UiD"].GetInt();

                    float currentPrice = data["price"].GetFloat();

                    int currentStatus = data["status"].GetInt();
                    MasterData masterElement(currentUid);
                    masterElement.setPrice(currentPrice);
                    masterElement.setStatus(static_cast<MasterData::Status>(currentStatus));
                    dataList.push_back(masterElement);
                }
            }
        }
        return dataList;
    }

    std::list<Item> ItemDataService(const rapidjson::Value &i_json)
    {
        std::list<Item> itemList{};

        if (i_json.IsObject() && i_json.HasMember("Item"))
        {
            const auto &itemsObj = i_json["Item"].GetArray();
            if (itemsObj.Size() == 0)
            {
                throw "No Item Data! ";
            }
            for (const auto &item : itemsObj)
            {

                if (item.HasMember("UiD") && item.HasMember("bricks"))
                {

                    int currentUiD = item["UiD"].GetInt();

                    Item currentItem(currentUiD);

                    std::list<Brick> currentBricks{};
                    const auto &bricksObj = item["bricks"].GetArray();
                    for (auto &brick : bricksObj)
                    {
                        currentBricks.push_back(brickParser(brick));
                    }
                    currentItem.setBricksInItem(currentBricks);
                    itemList.push_back(currentItem);
                }
            }
        }
        return itemList;
    }

    Item PreferredItemService(std::list<Brick> i_listOfBricks, std::list<MasterData> i_masterData, std::list<Item> i_itemList)
    {
        Item preferredItem;

        std::list<int> currentUiDs = matchBricksListToItem(i_listOfBricks, i_itemList);

        int currentUiD = findBestStatusPriceItem(i_masterData, currentUiDs);
        for (Item item : i_itemList)
        {
            if (item.getItemUiD() == currentUiD)
            {
                preferredItem = item;
            }
        }
        return preferredItem;
    }
}