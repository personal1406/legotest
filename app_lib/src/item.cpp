#include "item.hpp"
#include <iostream>

Item::Item() {}

Item::Item(int i_productUiD) { setItemUiD(i_productUiD); }

Item::Item(int i_productUiD, std::list<Brick> i_bricksList)
{
  setItemUiD(i_productUiD);
  setBricksInItem(i_bricksList);
}

Item::~Item() {}

void Item::setItemUiD(int i_productUiD) { m_UiD = i_productUiD; }

void Item::setBricksInItem(std::list<Brick> i_bricksList)
{
  m_bricksList = i_bricksList;
}

int Item::getItemUiD() { return m_UiD; }

std::list<Brick> Item::getBricksInItem()
{
  return m_bricksList;
}