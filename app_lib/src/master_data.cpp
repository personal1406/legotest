#include "master_data.hpp"

MasterData::MasterData(int i_itemUiD) { m_itemUiD = i_itemUiD; }

MasterData::~MasterData() {}

void MasterData::setPrice(float i_price) { m_price = i_price; }

void MasterData::setStatus(Status i_status) { m_status = i_status; }

float MasterData::getPrice() { return m_price; }

MasterData::Status MasterData::getStatus() { return m_status; }

int MasterData::getUiD() { return m_itemUiD; }
