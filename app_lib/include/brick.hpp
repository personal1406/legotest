#pragma once
#include <list>

class Brick {

public:
  enum ColorCode { RED = 20, BLUE = 21, GREEN = 22, WHITE = 23, YELLOW = 24 };

  Brick();
  Brick(int i_designId);
  Brick(int i_designId, std::list<int> i_brickColors);
  ~Brick();

  /// @brief Helper function to set colorCodes when a new brick is created, from
  /// a list of integers
  /// @param brickColors the list of integers defining the order and colors
  /// assigned to a new brick
  void setColorCodes(std::list<int> i_brickColors);

  /// @brief Helper function to set a designId when a new brick is created
  /// @param designId an integer specifying the new designId
  void setDesignId(int i_designId);

  /// @brief Helper function to get colorCodes associated to the current brick
  /// @return a list of colorCode, in the specific order assigned when created
  std::list<ColorCode> getColorCodes();

  /// @brief Helper function to get a design Id of the current brick
  /// @return the assigned designId
  int getDesignId();

private:
  int m_designId;
  std::list<ColorCode> m_colorCodes{};
};