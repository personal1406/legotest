#pragma once
#include "brick.hpp"
#include <list>

class Item {
public:
  Item();
  Item(int i_productUiD);
  Item(int i_productUiD, std::list<Brick> i_bricksList);
  ~Item();

  /// @brief Helper function to assign a Unique Identifier to a newly created
  /// Item
  /// @param productUiD a Unique identifier associated to the current Item
  void setItemUiD(int i_productUiD);

  /// @brief Helper function to assign a list of Bricks to a newly created Item
  /// @param bricksList list of bricks associted to the Item
  void setBricksInItem(std::list<Brick> i_bricksList);

  /// @brief Helper function to obtain the ItemUiD
  /// @return the Unique Idnetifier associated to the item
  int getItemUiD();

  /// @brief Helper function to obtain the list of bricks associated to the Item
  /// @return the list of Bricks associated to the Item
  std::list<Brick> getBricksInItem();

private:
  int m_UiD;
  std::list<Brick> m_bricksList{};
};