#pragma once

class MasterData {
public:
  enum Status { DISCONTINUED, OUTGOING, NOVELTY, NORMAL };

  MasterData(int i_itemUiD);
  ~MasterData();

  /// @brief Helper function to assign a Price to an associated item Uid
  /// @param i_price price to be used
  void setPrice(float i_price);

  /// @brief Helper function to assign a status to an associated item Uid
  /// @param i_status status for corresponding item
  void setStatus(Status i_status);

  /// @brief Helper function to get the price of an item
  /// @return the price associated to the item
  float getPrice();

  /// @brief Helper function to get the Status associated to an item
  /// @return the status
  Status getStatus();

  /// @brief Helper function to get the UiD of the current item
  /// @return  the UiD of the current item
  int getUiD();

private:
  int m_itemUiD;
  float m_price;
  Status m_status;
};