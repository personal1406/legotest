#include "item.hpp"
#include "brick.hpp"
#include "master_data.hpp"
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include <iostream>

namespace application
{

    /// @brief Helper function to read a json file
    /// @param fileName string containing file path
    /// @return rapidjson Document object with data to be loaded
    rapidjson::Document getJsonDoc(std::string fileName);

    /// @brief Helper function to parse a brick Json object
    /// @param i_json the json object containing brick data
    /// @return brick object
    Brick brickParser(const rapidjson::Value &i_json);

    /// @brief  Helper function that returns a list of item UiD matching a list of Bricks
    /// @param i_listOfBricks the list of bricks to search for
    /// @param i_itemList the list of items where to search
    /// @return the list of UiDs of items matching the search
    std::list<int> matchBricksListToItem(std::list<Brick> i_listOfBricks, std::list<Item> i_itemList);

    /// @brief  Helper function that returns the UiD among a possible list of items, in terms of best Status and lowest price
    /// @param i_masterData  Data where to search for current Status and price
    /// @param i_itemUiD The list of UiDs to compare
    /// @return the best UiD
    int findBestStatusPriceItem(std::list<MasterData> i_masterData, std::list<int> i_itemUiD);

    /// @brief  Helper function that creates a list of bricks for comparison from a json
    /// @param i_json input json object
    /// @return list of bricks
    std::list<Brick> parseListOfBricks(const rapidjson::Value &i_json);

    /// @brief Helper function that retrieves a list of MasterData (in this case
    /// assumed to be stored in a json)
    /// @return a list of MasterData
    std::list<MasterData> MasterDataService(const rapidjson::Value &i_json);

    /// @brief  Helper function that retrieves a list of Items (in this case assumed
    /// to be stored in a json file)
    /// @return a list of Items
    std::list<Item> ItemDataService(const rapidjson::Value &i_json);

    /// @brief Function that given a list of bricks to compare to, returns the
    /// PreferredItem, among the current Items
    /// @param i_listOfBricks input list of bricks to compare items to
    /// @return the PreferredItem
    Item PreferredItemService(std::list<Brick> i_listOfBricks, std::list<MasterData> i_masterData, std::list<Item> i_itemList);

};
