#include "app.hpp"

#define ASSERT(expr)                                                                    \
    if (!static_cast<bool>(expr))                                                       \
    {                                                                                   \
        std::cout << "Failed with: " << #expr << " at line: " << __LINE__ << std::endl; \
        exit(1);                                                                        \
    }

int main(int /*argc*/, char ** /*argv*/)
{
    std::cout << "In main" << std::endl;
    rapidjson::Document masterDataJson = application::getJsonDoc("../test/masterData.json");
    ASSERT(masterDataJson.IsObject());
    rapidjson::Document itemJson = application::getJsonDoc("../test/Item_copy.json");
    ASSERT(itemJson.IsObject());
    rapidjson::Document brickJson = application::getJsonDoc("../test/listOfBricks.json");
    ASSERT(brickJson.IsObject());

    std::list<MasterData> master = application::MasterDataService(masterDataJson);
    ASSERT(master.size());
    std::list<Item> items = application::ItemDataService(itemJson);
    ASSERT(items.size());
    std::list<Brick> listOfBricks = application::parseListOfBricks(brickJson);
    ASSERT(listOfBricks.size());

    std::cout << "Testing correct Item choice based on better Status" << std::endl;
    Item myItem = application::PreferredItemService(listOfBricks, master, items);
    ASSERT(myItem.getItemUiD() == 1010);

    std::cout << "Testing correct Item choice, given same status, lower price" << std::endl;
    rapidjson::Document brickJson1 = application::getJsonDoc("../test/listOfBricks1.json");
    std::list<Brick> listOfBricks1 = application::parseListOfBricks(brickJson1);
    Item myItem1 = application::PreferredItemService(listOfBricks1, master, items);
    ASSERT(myItem1.getItemUiD() == 9009);

    std::cout << "Testing correct Item choice, given different size of Bricks List" << std::endl;
    rapidjson::Document brickJson2 = application::getJsonDoc("../test/listOfBricks2.json");
    std::list<Brick> listOfBricks2 = application::parseListOfBricks(brickJson2);
    Item myItem2 = application::PreferredItemService(listOfBricks2, master, items);
    ASSERT(myItem2.getItemUiD() == 101);
    return 0;
}